export let timViTri = (glassId, arr) => {
  let viTri = -1;
  for (var index = 0; index < arr.length; index++) {
    var glass = arr[index];
    if (glass.id == glassId) {
      viTri = index;
      break;
    }
  }
  return viTri;
};
export let showKinhLen =(glass)=>{
    document.getElementById("name").innerHTML=glass.name
    document.getElementById("brand").innerHTML=glass.brand
    document.getElementById("color").innerHTML=glass.color
    document.getElementById("price").innerHTML=glass.price
    document.getElementById("description").innerHTML=glass.description
    document.getElementById("avatar").innerHTML=`<img class="d-none" id="glassInput" src="${glass.virtualImg}" >`
}
